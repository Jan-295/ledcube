# LedCube

This repository is a student research project from Jan Huber and Jonas Keller studying at DHBW in Friedrichshafen (coopertio company: Marquardt GmbH). The project is targeting on a Windows application programmed with c# .net5 Framework, that is a simple graphic programming interface (PI) for an LedCube (3x3x3). The goal is to create this PI so that primary school students can understand and use the programm. The hardware (LedCube) is based on an Arduino Nano and is realized and documented localy with KiCAD.


