﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LedCube
{
    public partial class Texteditor : RichTextBox
    {
        string _fileDirectory;
        string _sketchPath;
        string _defaultSketchPath;
        List<string> TexteditorLines;
        public Texteditor()
        {
            this.SelectionFont = new Font(this.SelectionFont.FontFamily, 16.0F);
            this.BackColor = Color.White;
            this.ForeColor = Color.Black;
            this.BorderStyle = BorderStyle.None;
            this.ScrollBars = RichTextBoxScrollBars.None;
#if DEBUG
            _fileDirectory = Directory.GetParent(Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).ToString()).ToString()).ToString() + @"\arduino-cli";
#else
            _fileDirectory = Directory.GetCurrentDirectory() + @"\arduino-cli";
#endif
            _sketchPath = _fileDirectory + @"\Sketch\Sketch.ino";
            _defaultSketchPath = _fileDirectory + @"\Sketch\defaultSketch.txt";
            InitializeComponent();
            this.SelectionFont = new Font(this.SelectionFont.FontFamily, 16.0F);
            loadDefaultFile();
        }

        public void loadDefaultFile()
        {
            string[] defaultLines = File.ReadAllLines(_defaultSketchPath);
            this.Lines = defaultLines;
        }
        public void changeTextSize(int Size)
        {
            this.SelectionFont = new Font("Tahoma", Size, FontStyle.Regular);
        }
    }
}
