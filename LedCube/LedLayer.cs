﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LedCube
{
    public partial class LedLayer : Panel
    {
        
        private bool[] b = new bool[9]; //initialized the button value array
        private Button[] btn = new Button[9]; //initialized the buttons array
        private int _padding = 4; //clearance between buttons
        private int _panelsize = 150; //size of the panel
        private Color _btnColorActive = Color.Green; //Color when button is active
        private Color _btnColorDeactive = Color.Gray; //Color when button is deactive
        public Color BtnColorActive { get { return _btnColorActive; } set { _btnColorActive = value; } }
        public Color BtnColorDecative { get { return _btnColorDeactive; } set { _btnColorDeactive = value; } }
        public int Panelsize { get { return _panelsize; } set { _panelsize = value; } }
        
        public LedLayer(int panelsize)
        {
            _panelsize = panelsize;
            this.Size = new Size(_panelsize, _panelsize);
            UpdateStyles();

            for (int t = 0; t < 9; t++) //this loop makes all the buttons assigned to a button
            {
                btn[t] = new Button();
            }
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
            InitializeComponent();
            startValueBtn(); //assign buttons settings
            btn[0].Click += new System.EventHandler(btn0Click); //assign click event to buttons
            btn[1].Click += new System.EventHandler(btn1Click);
            btn[2].Click += new System.EventHandler(btn2Click);
            btn[3].Click += new System.EventHandler(btn3Click);
            btn[4].Click += new System.EventHandler(btn4Click);
            btn[5].Click += new System.EventHandler(btn5Click);
            btn[6].Click += new System.EventHandler(btn6Click);
            btn[7].Click += new System.EventHandler(btn7Click);
            btn[8].Click += new System.EventHandler(btn8Click);
        }

        private void startValueBtn()
        {
            
            for (int t = 0; t < 9; t++) //this loop makes all the button values to false
            {
                b[t] = false;
            }
            
            int width, height;
            Point templocation = new Point(0, 0); //top left as startposition in the Panel
            width = this.Size.Width;
            height = this.Size.Height;
            width = (width - _padding * 2) / 3; //calculate suitable values for Button size and clearance
            height = (height -_padding * 2) / 3;
            
            for (int i = 0; i < 9; i++) //assign some necessary values to buttons and read the count numbers from memory
            {
                btn[i].Name = "btn_" + i; //the names are changed!
                btn[i].TabIndex = i;
                btn[i].Text = null;
                btn[i].Size = new Size(width, height);
                btn[i].Visible = true;
                btn[i].Parent = this;
                btn[i].FlatStyle = FlatStyle.Flat;
                btn[i].BackColor = _btnColorDeactive;
                btn[i].ForeColor = _btnColorDeactive;
                

            }
            //this lines sets the location of the buttons inside the panel which is the parent control
            btn[0].Location = templocation;
            templocation.X = width + _padding;
            btn[1].Location = templocation;
            templocation.X = (width + _padding) * 2;
            btn[2].Location = templocation;
            templocation.X = 0;
            templocation.Y = height + _padding;
            btn[3].Location = templocation;
            templocation.X = width + _padding;
            btn[4].Location = templocation;
            templocation.X = (width + _padding) * 2;
            btn[5].Location = templocation;
            templocation.Y = (height + _padding) * 2;
            templocation.X = 0;
            btn[6].Location = templocation;
            templocation.X = width + _padding;
            btn[7].Location = templocation;
            templocation.X = (width + _padding) * 2;
            btn[8].Location = templocation;

        }
        void changeBtn(Button[] btn, int index)
        {
            //common function to toggle color and state in button value array
            if (b[index])
            {
                btn[index].BackColor = _btnColorDeactive;
                btn[index].ForeColor = _btnColorDeactive;
                b[index] = false;
            }
            else
            {
                btn[index].BackColor = _btnColorActive;
                btn[index].ForeColor = _btnColorActive;
                b[index] = true;
            }
        }
        //here the click events start, they just call the changeBtn method to toggle color and state
        private void btn0Click(Object sender, EventArgs e)
        {
            changeBtn(btn, 0);
        }
        private void btn1Click(Object sender, EventArgs e)
        {
            changeBtn(btn, 1);
        }
        private void btn2Click(Object sender, EventArgs e)
        {
            changeBtn(btn, 2);
        }
        private void btn3Click(Object sender, EventArgs e)
        {
            changeBtn(btn, 3);
        }
        private void btn4Click(Object sender, EventArgs e)
        {
            changeBtn(btn, 4);
        }
        private void btn5Click(Object sender, EventArgs e)
        {
            changeBtn(btn, 5);
        }
        private void btn6Click(Object sender, EventArgs e)
        {
            changeBtn(btn, 6);
        }
        private void btn7Click(Object sender, EventArgs e)
        {
            changeBtn(btn, 7);
        }
        private void btn8Click(Object sender, EventArgs e)
        {
            changeBtn(btn, 8);
        }
        public string returnState()
        {
            //returns a String in binary format to later paste in arduino code
            string binaryNum = "0b";
            for(int i = 0; i < b.Length; i++)
            {
                if (b[i])
                {
                    binaryNum += "1";
                } else
                {
                    binaryNum += "0";
                }
            }
            return binaryNum;
        }
    }
}

