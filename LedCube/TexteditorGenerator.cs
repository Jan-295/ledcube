﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LedCube
{
    class TexteditorGenerator
    {
        private TabPage _tabpage;
        private RichTextBox lineNumbers = new RichTextBox();
        private Texteditor textEditor = new Texteditor();
        private int _lineNumbersWidth = 20;
        private int _writtenLines = 0;
        public TexteditorGenerator(TabPage tabpage)
        {
            textEditor.TextChanged += new EventHandler(textEditorChangedText);
            _tabpage = tabpage;
            _tabpage.Controls.Add(lineNumbers);
            lineNumbers.BorderStyle = BorderStyle.None;
            lineNumbers.Dock = DockStyle.Left;
            lineNumbers.Width = _lineNumbersWidth;
            lineNumbers.BackColor = Color.LightGray;
            lineNumbers.Cursor = Cursors.Default;
            lineNumbers.ReadOnly = true;
            _tabpage.Controls.Add(textEditor);
            textEditor.BorderStyle = BorderStyle.None;
            textEditor.Dock = DockStyle.Right;
            textEditor.Width = tabpage.Width - _lineNumbersWidth - 5;
            textEditor.BackColor = Color.White;
            //adding Numbers to List according to Lines in te textEditor
            _writtenLines = textEditor.Lines.Length;
            for (int i = 1; i <= _writtenLines; i++)
            {
                lineNumbers.AppendText(string.Format("{0}\n", i));
            }
        }
        public List<string> getText()
        {
            return textEditor.Lines.ToList();
        }

        public void loadDefaultFile()
        {
            textEditor.loadDefaultFile();
        }

        private void textEditorChangedText(object sender, EventArgs e)
        {
            if(textEditor.Lines.Length > _writtenLines)
            {
                _writtenLines = textEditor.Lines.Length;
                addNumberToList();
            } else if(textEditor.Lines.Length < _writtenLines)
            {
                _writtenLines = textEditor.Lines.Length;
                subNumberToList();
            }
        }

        private void addNumberToList(){
            lineNumbers.AppendText(string.Format("{0}\n", _writtenLines));
        }

        private void subNumberToList()
        {
            List<string> list = lineNumbers.Lines.ToList();
            list.RemoveAt(_writtenLines);
            lineNumbers.Lines = list.ToArray();
        }
       
    }
}
