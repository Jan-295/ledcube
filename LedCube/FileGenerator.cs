﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace LedCube
{
    class FileGenerator
    {
        private int _loopPos = 11; //Line where to start inserting Generated code
        string _fileDirectory;
        string _sketchPath;
        string _defaultSketchPath;
        List<LedLayer[]> _cubeSnap  = new List<LedLayer[]>();
        public FileGenerator(List<LedLayer[]> cubeSnap)
        {
            _cubeSnap = cubeSnap;
#if DEBUG
            _fileDirectory = Directory.GetParent(Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).ToString()).ToString()).ToString() + @"\arduino-cli";
#else
            _fileDirectory = Directory.GetCurrentDirectory() + @"\arduino-cli";
#endif
            _sketchPath = _fileDirectory + @"\Sketch\Sketch.ino";
            _defaultSketchPath = _fileDirectory + @"\Sketch\defaultSketch.txt";

            Debug.WriteLine("FileDirectory: {0}",_fileDirectory);
            reset();
        }
        public void reset()
        {
            //Copy default code from defaultSketch to real Sketch
            List<string> defaultLines = File.ReadAllLines(_defaultSketchPath).ToList();
            File.WriteAllLines(_sketchPath, defaultLines);
            _loopPos = 11;
        }
        public void generateTexteditorCode(List<string> texteditorText)
        {
            File.WriteAllLines(_sketchPath, texteditorText);
        }
        public void generateGraphicalCode(int waitTime, int lightIntensity)
        {
            List<string> sketch = File.ReadAllLines(_sketchPath).ToList();
            foreach(LedLayer[] layers in _cubeSnap)
            {
                Debug.WriteLine(layers[0].returnState());
                Debug.WriteLine(layers[1].returnState());
                Debug.WriteLine(layers[2].returnState());
                sketch.Insert(_loopPos, setCube(layers, lightIntensity));
                sketch.Insert(_loopPos + 1, endBlock(waitTime));
                _loopPos += 2;
            }
            File.WriteAllLines(_sketchPath, sketch);
        }
        public string setCube(LedLayer[] layers, int lightIntensity)
        {
            string _setCube = "cube.setCube(";
            _setCube += layers[0].returnState() + ",";
            _setCube += layers[1].returnState() + ",";
            _setCube += layers[2].returnState() + ",";
            _setCube += lightIntensity + ");";
            Debug.WriteLine(_setCube);

            return _setCube;
        }

        public string endBlock(int waitTime)
        {
            string _endBlock = "cube.endBlock(";
            _endBlock += waitTime + ");";
            return _endBlock;
        }
        
    }
}
