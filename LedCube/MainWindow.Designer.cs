﻿
namespace LedCube
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.materialTabControl1 = new MaterialSkin.Controls.MaterialTabControl();
            this.GraphicalTab = new System.Windows.Forms.TabPage();
            this.TexteditorTab = new System.Windows.Forms.TabPage();
            this.SettingsTab = new System.Windows.Forms.TabPage();
            this.ManualComSwitch = new MaterialSkin.Controls.MaterialSwitch();
            this.IconList = new System.Windows.Forms.ImageList(this.components);
            this.circularProgressBar1 = new CircularProgressBar_NET5.CircularProgressBar();
            this.Upload = new MaterialSkin.Controls.MaterialButton();
            this.ManualComSelector = new MaterialSkin.Controls.MaterialComboBox();
            this.SpeedButton = new MaterialSkin.Controls.MaterialButton();
            this.LightButton = new MaterialSkin.Controls.MaterialButton();
            this.ClearButton = new MaterialSkin.Controls.MaterialButton();
            this.materialTabControl1.SuspendLayout();
            this.SettingsTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // materialTabControl1
            // 
            this.materialTabControl1.Controls.Add(this.GraphicalTab);
            this.materialTabControl1.Controls.Add(this.TexteditorTab);
            this.materialTabControl1.Controls.Add(this.SettingsTab);
            this.materialTabControl1.Depth = 0;
            this.materialTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialTabControl1.ImageList = this.IconList;
            this.materialTabControl1.Location = new System.Drawing.Point(3, 66);
            this.materialTabControl1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabControl1.Multiline = true;
            this.materialTabControl1.Name = "materialTabControl1";
            this.materialTabControl1.SelectedIndex = 0;
            this.materialTabControl1.Size = new System.Drawing.Size(1914, 962);
            this.materialTabControl1.TabIndex = 2;
            // 
            // GraphicalTab
            // 
            this.GraphicalTab.AutoScroll = true;
            this.GraphicalTab.BackColor = System.Drawing.Color.White;
            this.GraphicalTab.ForeColor = System.Drawing.SystemColors.Control;
            this.GraphicalTab.ImageKey = "cursorIcon.png";
            this.GraphicalTab.Location = new System.Drawing.Point(4, 39);
            this.GraphicalTab.Name = "GraphicalTab";
            this.GraphicalTab.Padding = new System.Windows.Forms.Padding(3);
            this.GraphicalTab.Size = new System.Drawing.Size(1906, 919);
            this.GraphicalTab.TabIndex = 0;
            this.GraphicalTab.Text = "Graphical";
            this.GraphicalTab.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // TexteditorTab
            // 
            this.TexteditorTab.BackColor = System.Drawing.Color.White;
            this.TexteditorTab.ImageKey = "programmingIcon.png";
            this.TexteditorTab.Location = new System.Drawing.Point(4, 39);
            this.TexteditorTab.Name = "TexteditorTab";
            this.TexteditorTab.Padding = new System.Windows.Forms.Padding(3);
            this.TexteditorTab.Size = new System.Drawing.Size(1906, 919);
            this.TexteditorTab.TabIndex = 1;
            this.TexteditorTab.Text = "Texteditor";
            // 
            // SettingsTab
            // 
            this.SettingsTab.Controls.Add(this.ManualComSwitch);
            this.SettingsTab.ImageKey = "settingsIcon.png";
            this.SettingsTab.Location = new System.Drawing.Point(4, 39);
            this.SettingsTab.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SettingsTab.Name = "SettingsTab";
            this.SettingsTab.Size = new System.Drawing.Size(1906, 919);
            this.SettingsTab.TabIndex = 2;
            this.SettingsTab.Text = "Settings";
            this.SettingsTab.UseVisualStyleBackColor = true;
            // 
            // ManualComSwitch
            // 
            this.ManualComSwitch.AutoSize = true;
            this.ManualComSwitch.BackColor = System.Drawing.Color.White;
            this.ManualComSwitch.Depth = 0;
            this.ManualComSwitch.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ManualComSwitch.Location = new System.Drawing.Point(138, 35);
            this.ManualComSwitch.Margin = new System.Windows.Forms.Padding(0);
            this.ManualComSwitch.MouseLocation = new System.Drawing.Point(-1, -1);
            this.ManualComSwitch.MouseState = MaterialSkin.MouseState.HOVER;
            this.ManualComSwitch.Name = "ManualComSwitch";
            this.ManualComSwitch.Ripple = true;
            this.ManualComSwitch.Size = new System.Drawing.Size(253, 37);
            this.ManualComSwitch.TabIndex = 0;
            this.ManualComSwitch.Text = "Manual COM Port Selection";
            this.ManualComSwitch.UseVisualStyleBackColor = false;
            this.ManualComSwitch.CheckedChanged += new System.EventHandler(this.manualComSwitch_CheckedChanged);
            // 
            // IconList
            // 
            this.IconList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.IconList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IconList.ImageStream")));
            this.IconList.TransparentColor = System.Drawing.Color.Transparent;
            this.IconList.Images.SetKeyName(0, "cursorIcon.png");
            this.IconList.Images.SetKeyName(1, "programmingIcon.png");
            this.IconList.Images.SetKeyName(2, "settingsIcon.png");
            // 
            // circularProgressBar1
            // 
            this.circularProgressBar1.AnimationFunction = WinFormAnimation_NET5.KnownAnimationFunctions.Linear;
            this.circularProgressBar1.AnimationSpeed = 500;
            this.circularProgressBar1.BackColor = System.Drawing.Color.White;
            this.circularProgressBar1.Font = new System.Drawing.Font("Segoe UI", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.circularProgressBar1.ForeColor = System.Drawing.Color.White;
            this.circularProgressBar1.InnerColor = System.Drawing.Color.White;
            this.circularProgressBar1.InnerMargin = 2;
            this.circularProgressBar1.InnerWidth = -1;
            this.circularProgressBar1.Location = new System.Drawing.Point(1779, 32);
            this.circularProgressBar1.MarqueeAnimationSpeed = 2000;
            this.circularProgressBar1.Name = "circularProgressBar1";
            this.circularProgressBar1.OuterColor = System.Drawing.Color.White;
            this.circularProgressBar1.OuterMargin = 0;
            this.circularProgressBar1.OuterWidth = 0;
            this.circularProgressBar1.ProgressColor = System.Drawing.Color.Black;
            this.circularProgressBar1.ProgressWidth = 8;
            this.circularProgressBar1.SecondaryFont = new System.Drawing.Font("Segoe UI", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.circularProgressBar1.Size = new System.Drawing.Size(36, 36);
            this.circularProgressBar1.StartAngle = 270;
            this.circularProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.circularProgressBar1.SubscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularProgressBar1.SubscriptMargin = new System.Windows.Forms.Padding(10, -35, 0, 0);
            this.circularProgressBar1.SubscriptText = ".23";
            this.circularProgressBar1.SuperscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularProgressBar1.SuperscriptMargin = new System.Windows.Forms.Padding(10, 35, 0, 0);
            this.circularProgressBar1.SuperscriptText = "";
            this.circularProgressBar1.TabIndex = 0;
            this.circularProgressBar1.TextMargin = new System.Windows.Forms.Padding(8, 8, 0, 0);
            this.circularProgressBar1.Value = 68;
            // 
            // Upload
            // 
            this.Upload.AccentTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(64)))), ((int)(((byte)(129)))));
            this.Upload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Upload.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Upload.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.Upload.Depth = 0;
            this.Upload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Upload.HighEmphasis = true;
            this.Upload.Icon = null;
            this.Upload.Location = new System.Drawing.Point(1815, 32);
            this.Upload.Margin = new System.Windows.Forms.Padding(4);
            this.Upload.MouseState = MaterialSkin.MouseState.HOVER;
            this.Upload.Name = "Upload";
            this.Upload.NoAccentTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.Upload.Size = new System.Drawing.Size(78, 36);
            this.Upload.TabIndex = 2;
            this.Upload.Text = "Upload";
            this.Upload.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Text;
            this.Upload.UseAccentColor = false;
            this.Upload.UseVisualStyleBackColor = true;
            this.Upload.Click += new System.EventHandler(this.upload_Click);
            // 
            // ManualComSelector
            // 
            this.ManualComSelector.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ManualComSelector.AutoResize = false;
            this.ManualComSelector.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ManualComSelector.Depth = 0;
            this.ManualComSelector.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.ManualComSelector.DropDownHeight = 432;
            this.ManualComSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ManualComSelector.DropDownWidth = 121;
            this.ManualComSelector.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ManualComSelector.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.ManualComSelector.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ManualComSelector.FormattingEnabled = true;
            this.ManualComSelector.IntegralHeight = false;
            this.ManualComSelector.ItemHeight = 43;
            this.ManualComSelector.Location = new System.Drawing.Point(1463, 32);
            this.ManualComSelector.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ManualComSelector.MaxDropDownItems = 10;
            this.ManualComSelector.MouseState = MaterialSkin.MouseState.OUT;
            this.ManualComSelector.Name = "ManualComSelector";
            this.ManualComSelector.Size = new System.Drawing.Size(142, 49);
            this.ManualComSelector.StartIndex = 0;
            this.ManualComSelector.TabIndex = 3;
            // 
            // SpeedButton
            // 
            this.SpeedButton.AccentTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(64)))), ((int)(((byte)(129)))));
            this.SpeedButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SpeedButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SpeedButton.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.SpeedButton.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.SpeedButton.Depth = 0;
            this.SpeedButton.HighEmphasis = true;
            this.SpeedButton.Icon = global::LedCube.Properties.Resources.flash_1;
            this.SpeedButton.Location = new System.Drawing.Point(1362, 32);
            this.SpeedButton.Margin = new System.Windows.Forms.Padding(4);
            this.SpeedButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.SpeedButton.Name = "SpeedButton";
            this.SpeedButton.NoAccentTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.SpeedButton.Size = new System.Drawing.Size(94, 36);
            this.SpeedButton.TabIndex = 4;
            this.SpeedButton.Text = "Speed";
            this.SpeedButton.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Text;
            this.SpeedButton.UseAccentColor = false;
            this.SpeedButton.UseVisualStyleBackColor = false;
            this.SpeedButton.Click += new System.EventHandler(this.speedButton_Click);
            // 
            // LightButton
            // 
            this.LightButton.AccentTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(64)))), ((int)(((byte)(129)))));
            this.LightButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LightButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.LightButton.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.LightButton.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.LightButton.Depth = 0;
            this.LightButton.HighEmphasis = true;
            this.LightButton.Icon = global::LedCube.Properties.Resources.light_3;
            this.LightButton.Location = new System.Drawing.Point(1185, 32);
            this.LightButton.Margin = new System.Windows.Forms.Padding(4);
            this.LightButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.LightButton.Name = "LightButton";
            this.LightButton.NoAccentTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.LightButton.Size = new System.Drawing.Size(148, 36);
            this.LightButton.TabIndex = 5;
            this.LightButton.Text = "LightButton";
            this.LightButton.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Text;
            this.LightButton.UseAccentColor = false;
            this.LightButton.UseVisualStyleBackColor = false;
            this.LightButton.Click += new System.EventHandler(this.lightButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.AccentTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(64)))), ((int)(((byte)(129)))));
            this.ClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ClearButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClearButton.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.ClearButton.Depth = 0;
            this.ClearButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClearButton.HighEmphasis = true;
            this.ClearButton.Icon = null;
            this.ClearButton.Location = new System.Drawing.Point(1629, 32);
            this.ClearButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.ClearButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.NoAccentTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.ClearButton.Size = new System.Drawing.Size(66, 36);
            this.ClearButton.TabIndex = 6;
            this.ClearButton.Text = "Clear";
            this.ClearButton.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Text;
            this.ClearButton.UseAccentColor = false;
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1920, 1030);
            this.Controls.Add(this.circularProgressBar1);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.LightButton);
            this.Controls.Add(this.SpeedButton);
            this.Controls.Add(this.ManualComSelector);
            this.Controls.Add(this.Upload);
            this.Controls.Add(this.materialTabControl1);
            this.DrawerTabControl = this.materialTabControl1;
            this.DrawerWidth = 250;
            this.FormStyle = MaterialSkin.Controls.MaterialForm.FormStyles.ActionBar_64;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1920, 1080);
            this.MinimumSize = new System.Drawing.Size(1918, 1030);
            this.Name = "MainWindow";
            this.Padding = new System.Windows.Forms.Padding(3, 66, 3, 2);
            this.Sizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "LedCube";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.materialTabControl1.ResumeLayout(false);
            this.SettingsTab.ResumeLayout(false);
            this.SettingsTab.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialTabControl materialTabControl1;
        private System.Windows.Forms.TabPage GraphicalTab;
        private System.Windows.Forms.TabPage TexteditorTab;
        private MaterialSkin.Controls.MaterialButton Upload;
        private System.Windows.Forms.TabPage SettingsTab;
        private MaterialSkin.Controls.MaterialSwitch ManualComSwitch;
        private System.Windows.Forms.ImageList IconList;
        private MaterialSkin.Controls.MaterialComboBox ManualComSelector;
        private MaterialSkin.Controls.MaterialButton SpeedButton;
        private MaterialSkin.Controls.MaterialButton LightButton;
        private MaterialSkin.Controls.MaterialButton ClearButton;
        private CircularProgressBar_NET5.CircularProgressBar circularProgressBar1;
    }
}