﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace LedCube
{
    public partial class ArduinoUpload
    {
        public ProcessStartInfo arduinoCli;
        public string SketchDir;

        public string FileDirectory { get; private set; }
                
        public ArduinoUpload()
        {
            FileDirectory = "";
        }

        private string debugDir()
        {
            string DirCli;
            DirCli = Directory.GetCurrentDirectory() + @"\arduino-cli";
            for (int i = 0; i <= 3; i++)
            {
                DirCli = Directory.GetParent(DirCli).ToString();
            }
            FileDirectory = DirCli;
            DirCli += @"\arduino-cli";
            return DirCli;
        }

        private string appDir()
        {
            FileDirectory = Directory.GetCurrentDirectory();
            return Directory.GetCurrentDirectory() + @"\arduino-cli";
        }

        public void setup(bool showConsole)
        {
            string Dir;
#if DEBUG
            Dir = debugDir();
#else
            Dir = AppDir();
#endif
            SketchDir = "\"" + Dir + @"\Sketch\Sketch.ino";
            Debug.WriteLine(Dir);
            string EnvironmentVar = Environment.GetEnvironmentVariable("Path", EnvironmentVariableTarget.Process);
            if (!EnvironmentVar.Contains(Dir))
            {
                EnvironmentVar += Dir + ";";
                Environment.SetEnvironmentVariable("Path", EnvironmentVar, EnvironmentVariableTarget.Process);
            }

            arduinoCli = new ProcessStartInfo
            {
                CreateNoWindow = !showConsole,
                FileName = "cmd.exe",
                UseShellExecute = false,
            };
            ProcessStartInfo arduinoAvr;
            arduinoAvr = new ProcessStartInfo
            {
                CreateNoWindow = !showConsole,
                FileName = "cmd.exe",
                Arguments = @"/c arduino-cli core install arduino:avr",
                UseShellExecute = false,
            };
            var arduinoAvrProcess = Process.Start(arduinoAvr);
            while (!arduinoAvrProcess.HasExited)
            {
                Debug.WriteLine("In Setup");
            }
            Debug.WriteLine("Setup done");
        }

        public void upload(string comPort)
        {
            string Dir;
#if DEBUG
            Dir = debugDir();
#else
            Dir = AppDir();
#endif
            arduinoCli.Arguments = @"/c arduino-cli compile -p " + comPort + @" -b arduino:avr:nano:cpu=atmega328old --upload " + "\"" + Dir + @"\Sketch\Sketch.ino" + "\"";
            var arduinoCliProcess = Process.Start(arduinoCli);
            
            arduinoCliProcess.WaitForExit();
            
            Debug.WriteLine("Upload success");

        }
        public string autoComSelect()
        {
            string[] coms = getComPorts();
            if (coms.Length <= 0 || coms[0] == "Nothing connected") { return null; }
            foreach(string com in coms)
            {
                SerialPort serialPort = new SerialPort();
                serialPort.Close();
                serialPort.PortName = com;
                if(serialPort.IsOpen) serialPort.Close();
                serialPort.Close();
                try
                {             
                    serialPort.BaudRate = 115200;
                    serialPort.ReadTimeout = 3000;
                    serialPort.Open();
                    string response = serialPort.ReadLine();
                    serialPort.Close();
                    if(response.Contains("<LedCube>"))
                    {
                        serialPort.Dispose();
                        return com;
                    }
                }
                catch(Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
                serialPort.Dispose();
            }
            return null;
        }

        public string[] getComPorts()
        {
            string[] coms = SerialPort.GetPortNames();
            if (coms.Length == 0)
            {
               string[] Nocoms = {"No COM"};
                return Nocoms;
            }            
            return coms;          
        } 
    }
}
