﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LedCube
{
    class CubeLayersGenerator
    {
        private Point _tmpPos;
        private Point _DelButtonPos;
        private Point _InfoButtonPos;
        public List<LedLayer[]> cubeSnap = new List<LedLayer[]>();
        public List<MaterialSkin.Controls.MaterialFloatingActionButton> DelButtons = new List<MaterialSkin.Controls.MaterialFloatingActionButton>();
        public List<MaterialSkin.Controls.MaterialFloatingActionButton> InfoButtons = new List<MaterialSkin.Controls.MaterialFloatingActionButton>();
        private TabPage _tabpage;
        private MaterialSkin.Controls.MaterialFloatingActionButton AddButton = new();
        private int _layerPanelSize;
        private int _tabHeight;
        private FileGenerator _fileGenerator;
        private int _lightIntensity;
        private int _waitTime;

        public CubeLayersGenerator(TabPage tabpage, int tabHeight, int layerPanelSize, ref int lightIntensity, ref int waitTime)
        {
            _tabpage = tabpage;
            _tabHeight = tabHeight;
            _layerPanelSize = layerPanelSize;
            _lightIntensity = lightIntensity;
            _waitTime = waitTime;
            _tmpPos = new Point(_layerPanelSize, 20);
            _DelButtonPos = new Point(0,0);
            _InfoButtonPos = new Point(0,0);
            AddButton.AutoSize = false;
            AddButton.Icon = Properties.Resources.Add_white_16x;
            AddButton.Click += new EventHandler(addButton_Clicked);
            addNew();
            refreshLocation(_tabHeight);
        }
        public void clear()
        {
            delControls();
            cubeSnap.Clear();
            DelButtons.Clear();
            InfoButtons.Clear();
            addNew();
            refreshLocation(_tabHeight);
        }

        private void delButton_Clicked(object sender, EventArgs e)
        {
            
            MaterialSkin.Controls.MaterialFloatingActionButton Button = (MaterialSkin.Controls.MaterialFloatingActionButton)(sender as Button);

            if (cubeSnap.Count > 1)
            {
                delControls();
                cubeSnap.RemoveAt(Int32.Parse(Button.Name));
                DelButtons.RemoveAt(Int32.Parse(Button.Name));
                InfoButtons.RemoveAt(Int32.Parse(Button.Name));

                int ctr = 0;
                foreach (var b in DelButtons)
                {
                    b.Name = ctr.ToString();
                    ctr++;
                }

                ctr = 0;
                foreach (var b in InfoButtons)
                {
                    b.Name = ctr.ToString();
                    ctr++;
                }

                addControls();
                refreshLocation(_tabHeight);
            }
        }

        private void infoButton_Clicked(object sender, EventArgs e)
        {
            MaterialSkin.Controls.MaterialFloatingActionButton Button = (MaterialSkin.Controls.MaterialFloatingActionButton)(sender as Button);
            _fileGenerator = new FileGenerator(cubeSnap);
            string text_ = "Code for this Cube Instance:\n"+_fileGenerator.setCube(cubeSnap[Int32.Parse(Button.Name)], _lightIntensity) + "\n" + _fileGenerator.endBlock(_waitTime);
            Debug.WriteLine(text_);
            MessageBox.Show(text_);
            _fileGenerator = null; //destroy object
        }

        private void addButton_Clicked(object sender, EventArgs e)
        {
            if(cubeSnap.Count < 100)
            {
                addNew();
                refreshLocation(_tabHeight);
            }
        }

        public void addNew()
        {
            MaterialSkin.Controls.MaterialFloatingActionButton DelButton = new();
            DelButton.Icon = Properties.Resources.Del_white_16x;
            DelButton.Name = DelButtons.Count.ToString();
            DelButton.FlatStyle = FlatStyle.Flat;

            MaterialSkin.Controls.MaterialFloatingActionButton InfoButton = new();
            InfoButton.Icon = Properties.Resources.info;
            InfoButton.Name = InfoButtons.Count.ToString();
            InfoButton.FlatStyle = FlatStyle.Flat;

            DelButton.Click += new EventHandler(delButton_Clicked);
            InfoButton.Click += new EventHandler(infoButton_Clicked);

            LedLayer[] tempLayers = new LedLayer[3];
            for(int i = 0; i < tempLayers.Length; i++)
            {
                tempLayers[i] = new LedLayer(_layerPanelSize);
                tempLayers[i].Size = new Size(_layerPanelSize, _layerPanelSize);
                tempLayers[i].Location = _tmpPos;
                _tmpPos.Y += _tabHeight/4;
            }
            
            _tmpPos = new Point(_layerPanelSize, _layerPanelSize);

            InfoButtons.Add(InfoButton);
            DelButtons.Add(DelButton);
            cubeSnap.Add(tempLayers);
            addControls();
        }

        public void refreshLocation(int tabHeight)
        {
            int _padding = (tabHeight - 3 * _layerPanelSize) / 6;
            AddButton.Location = new Point((cubeSnap.Count * _layerPanelSize * 2) + _layerPanelSize - 30, _tabHeight / 2 - 20);

            foreach (LedLayer[] l in cubeSnap)
            {
                _tmpPos.Y = _padding;
                for (int i = 0; i < l.Length; i++)
                {
                    l[i].Location = _tmpPos;
                    _tmpPos.Y += 2 * _padding + _layerPanelSize;
                }
                _tmpPos.X += _layerPanelSize * 2;
            }

            _DelButtonPos = new Point(_layerPanelSize + (_layerPanelSize / 2) - 25, _tabHeight - 60);
            foreach (var buttons in DelButtons)
            {
                buttons.Location = _DelButtonPos;
                _DelButtonPos.X += _layerPanelSize * 2;
            }
            _DelButtonPos = new Point(_layerPanelSize + (_layerPanelSize / 2) - 25, _tabHeight - 60);

            _InfoButtonPos = new Point(_layerPanelSize + (_layerPanelSize / 2) - 25, 15);
            foreach (var buttons in InfoButtons)
            {
                buttons.Location = _InfoButtonPos;
                _InfoButtonPos.X += _layerPanelSize * 2;
            }
            _InfoButtonPos = new Point(_layerPanelSize + (_layerPanelSize / 2) - 25, 15);

            _tmpPos = new Point(_layerPanelSize, _layerPanelSize / 2);
        }
        private void addControls()
        {
            _tabpage.Controls.Add(AddButton);
            foreach (var button in DelButtons)
            {
                _tabpage.Controls.Add(button);
            }
            foreach (var button in InfoButtons)
            {
                _tabpage.Controls.Add(button);
            }
            foreach (var cubes in cubeSnap)
            {
                foreach(var layers in cubes)
                {
                    _tabpage.Controls.Add(layers);
                }
            }
        }
        private void delControls()
        {
            _tabpage.Controls.Clear();
        }

    }
}
