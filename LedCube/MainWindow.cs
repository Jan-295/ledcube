﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Threading;
using MaterialSkin.Controls;

namespace LedCube
{
    public partial class MainWindow : MaterialForm
    {
        
        public ArduinoUpload uploader;
        private CubeLayersGenerator cubeGenerator;
        private FileGenerator fileGenerator;
        private TexteditorGenerator texteditorGenerator;
        readonly MaterialSkin.MaterialSkinManager materialSkinManager;
        BackgroundWorker backgroundWorkerUpload = new BackgroundWorker();
        private string globalLedCubeCom = "";

        private int speedIconIndex = 0;
        private int[] speedVector = {2000, 1000, 500};
        private List<Image> speedIcons = new List<Image> {
        Properties.Resources.flash_1,
        Properties.Resources.flash_2,
        Properties.Resources.flash_3};

        public int lightIconIndex = 0;
        public int[] lightVector = {100, 75, 45};
        private List<Image> lightIcons = new List<Image> {
        Properties.Resources.light_3,
        Properties.Resources.light_2,
        Properties.Resources.light_1};
        

        public MainWindow(ArduinoUpload loader)
        {
            uploader = loader;

            InitializeComponent();

            circularProgressBar1.ProgressColor = Color.FromArgb(255, 104, 180, 244);
            circularProgressBar1.Hide();

            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.WindowState = FormWindowState.Maximized;
            this.MinimumSize = Screen.PrimaryScreen.WorkingArea.Size;
            this.MaximumSize = Screen.PrimaryScreen.WorkingArea.Size;

            Upload.Location = new Point(1800,15);
            ManualComSelector.Location = new Point(1500, 15);
            SpeedButton.Location = new Point(1300, 15);
            LightButton.Location = new Point(1100, 15);
            ClearButton.Location = new Point(1000, 15);
            circularProgressBar1.Location = new Point(Upload.Location.X-circularProgressBar1.Width, circularProgressBar1.Height+1);

            manualCom();
            updateComs();
            materialSkinManager = MaterialSkin.MaterialSkinManager.Instance;
            materialSkinManager.EnforceBackcolorOnAllComponents = true;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkin.MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new MaterialSkin.ColorScheme(MaterialSkin.Primary.Blue300, MaterialSkin.Primary.Blue500, MaterialSkin.Primary.Grey400, MaterialSkin.Accent.Blue200, MaterialSkin.TextShade.WHITE);

            texteditorGenerator = new TexteditorGenerator(TexteditorTab);

            cubeGenerator = new CubeLayersGenerator(GraphicalTab, GraphicalTab.Height, 125, ref lightVector[lightIconIndex], ref speedVector[speedIconIndex]);
            resizeElements();
            this.SizeChanged += new EventHandler(this.windowSizeChanged);
            ManualComSelector.Click += new EventHandler(this.manualComSelector_Click);
            materialTabControl1.SelectedIndexChanged += new EventHandler(this.tabChanged);
            fileGenerator = new FileGenerator(cubeGenerator.cubeSnap);

            backgroundWorkerUpload.DoWork += backgroundWorker_DoWork;
            backgroundWorkerUpload.RunWorkerCompleted += backgroundWorker_Done;
            
        }

        private void backgroundWorker_Done(object sender, RunWorkerCompletedEventArgs e)
        {
            circularProgressBar1.Hide();
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Debug.WriteLine("In Backgroundworker");

            if (globalLedCubeCom != null)
            {
                uploader.upload(globalLedCubeCom);
            }
            else
            {
                MessageBox.Show("No LedCube connected!\nMake sure a LedCube is connected with USB.");
            }
        }

        private void tabChanged(object sender, EventArgs e)
        {
            switch (materialTabControl1.SelectedTab.Name)
            {
                case "SettingsTab":
                    Upload.Enabled = false;
                    SpeedButton.Hide();
                    LightButton.Hide();
                    ClearButton.Hide();
                    return;
                case "TexteditorTab":
                    Upload.Enabled = true;
                    SpeedButton.Hide();
                    LightButton.Hide();
                    ClearButton.Show();
                    break;
                case "GraphicalTab":
                    Upload.Enabled = true;
                    SpeedButton.Show();
                    LightButton.Show();
                    ClearButton.Show();
                    break;
                default:
                    return;
            }
        }

        //%%%%%%%%%%%%%%%%%%%%%%-- Upload and COM selection --%%%%%%%%%%%%%%%%%%%%%%
        private void windowSizeChanged(object sender, System.EventArgs e)
        {
            resizeElements();
        }
        private void resizeElements()
        {
            int tabHeight = GraphicalTab.Height;
            cubeGenerator.refreshLocation(GraphicalTab.Height);    
        }
        private void manualComSelector_Click(object sender, EventArgs e)
        {
            updateComs();
        }
        private void updateComs()
        {
            ManualComSelector.Items.Clear();
            SerialPort.GetPortNames();
            
            var coms = uploader.getComPorts();
            foreach (string s in coms)
            {
                ManualComSelector.Items.Add(s);
            }
        }
        private void upload_Click(object sender, EventArgs e)
        {
            string LedCubeCom = "";
            switch (materialTabControl1.SelectedTab.Name)
            {
                case "GraphicalTab":
                    fileGenerator.reset();
                    fileGenerator.generateGraphicalCode(speedVector[speedIconIndex], lightVector[lightIconIndex]);
                    break;
                case "TexteditorTab":
                    fileGenerator.reset();
                    fileGenerator.generateTexteditorCode(texteditorGenerator.getText());
                    break;
                default:
                    return;
            }

            if (ManualComSwitch.Checked)
            {
                if (ManualComSelector.Items[0].ToString() == "No COM")
                {
                    LedCubeCom = null;
                }
                else
                {
                    LedCubeCom = ManualComSelector.SelectedItem.ToString();
                    Debug.WriteLine(LedCubeCom);
                }

            }
            else
            {
                LedCubeCom = uploader.autoComSelect();
            }
            Debug.WriteLine(LedCubeCom);
            globalLedCubeCom = LedCubeCom;
            circularProgressBar1.Show();
            backgroundWorkerUpload.RunWorkerAsync();
            
        }
        //%%%%%%%%%%%%%%%%%%%%%%-- Graphical Programming Tab --%%%%%%%%%%%%%%%%%%%%%%
        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
        private void manualCom()
        {
            if (ManualComSwitch.Checked)
            {
                ManualComSelector.Show();
            }
            else
            {
                ManualComSelector.Hide();
            }
        }

        private void manualComSwitch_CheckedChanged(object sender, EventArgs e)
        {
            manualCom();
        }

        private void speedButton_Click(object sender, EventArgs e)
        {
            if(speedIconIndex == speedIcons.Count - 1)
            {
                speedIconIndex = 0;
            } else {
                speedIconIndex++;
            }
            SpeedButton.Icon = speedIcons[speedIconIndex];
        }

        private void lightButton_Click(object sender, EventArgs e)
        {
            if (lightIconIndex == lightIcons.Count - 1)
            {
                lightIconIndex = 0;
            }
            else
            {
                lightIconIndex++;
            }
            LightButton.Icon = lightIcons[lightIconIndex];
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            switch (materialTabControl1.SelectedTab.Name)
            {
                case "GraphicalTab":
                    cubeGenerator.clear();
                    break;
                case "TexteditorTab":
                    texteditorGenerator.loadDefaultFile();
                    break;
                default:
                    return;
            }
        }
    }
}
