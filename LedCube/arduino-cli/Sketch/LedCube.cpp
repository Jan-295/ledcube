#include "LedCube.h"
#include <TimerOne.h>

volatile unsigned long millisecond = 0;
volatile unsigned long messageTime = 1000; //1000ms Serial message send

LedCube::LedCube(){  
    for(uint8_t i = 0; i < 9; i++){
      pinMode(columns[i],OUTPUT);
      digitalWrite(columns[i],LOW);
    }
  }
  
  void milliSek(){
    millisecond++;
    if(millisecond % messageTime == 0){
      Serial.println("<LedCube>");
      millisecond = 0;
    }
  }

 void LedCube::setup(){
    Serial.begin(115200);
    Timer1.initialize(1000); //Zeit in us
    Timer1.attachInterrupt(milliSek);
    Timer1.pwm(L1,0);
    Timer1.pwm(L2,0);
  }
  
  void LedCube::Led(uint8_t pin, uint8_t layer, uint8_t light){
    digitalWrite(pin, HIGH);
    if(layer == L3){
      analogWrite(L3, (int)255*((float)light/100));
    }else{
      Timer1.pwm(layer, (int)1023*((float)light/100));
    }
  }

  void LedCube::setLayer(uint16_t pinMask, uint8_t layer, uint8_t light){
    int mask = 0b100000000;
    for(int ctr = 0; ctr < 9; ctr++){
      if(pinMask & mask){
        Led(columns[ctr],layers[layer-1],light);
      }
      mask = mask >> 1;
    }
  }

  void LedCube::setCube(uint16_t pinMaskLayer1, uint16_t pinMaskLayer2, uint16_t pinMaskLayer3, uint8_t light){
  setLayer(pinMaskLayer1, 1, light);
  setLayer(pinMaskLayer2, 2, light);
  setLayer(pinMaskLayer3, 3, light);
 }
  
  void LedCube::test(){
    for(int i = 0; i < 9; i++){
      Serial.println(columns[i]);
    }
  }

  void LedCube::endBlock(uint16_t waitTime){
    wait(waitTime/2);
    allOff();
    wait(waitTime/2);
  }
  void LedCube::wait(uint16_t waitTime){
    delay(waitTime);
  }
  void LedCube::allOff(){
    for(uint8_t i = 0; i <= 8; i++){
      digitalWrite(columns[i], LOW);
    }
    Timer1.pwm(L1,0);
    Timer1.pwm(L2,0);
    analogWrite(L3, 0);
  }

  void LedCube::checkMask(uint16_t pinMask){
    if(pinMask>0b11111){
      //#error: pinMask size is too big max size is a 9 bit value
    }
  }
