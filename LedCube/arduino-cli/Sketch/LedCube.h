#include "Arduino.h"

#define C1 12
#define C2 8
#define C3 7
#define C4 6
#define C5 5
#define C6 4
#define C7 3
#define C8 2
#define C9 13

#define L1 9
#define L2 10
#define L3 11

#define CubeSize 3

class LedCube {
  
public:
  LedCube();
  void setup();
  void Led(uint8_t pin, uint8_t layer, uint8_t light);
  void setLayer(uint16_t pinMask, uint8_t layer, uint8_t light);
  void setCube(uint16_t pinMaskLayer1, uint16_t pinMaskLayer2, uint16_t pinMaskLayer3, uint8_t light);
  void endBlock(uint16_t waitTime);
  void test();
  
private:
  int layers[3] = {L1,L2,L3};
  int columns[9] = {C1,C2,C3,C4,C5,C6,C7,C8,C9};
  void wait(uint16_t waitTime);
  void allOff();
  void checkMask(uint16_t pinMask);
};
