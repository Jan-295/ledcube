﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LedCube
{
    public partial class SplashScreen : Form
    {
        BackgroundWorker backgroundWorker = new BackgroundWorker();
        ArduinoUpload uploader = new ArduinoUpload();
        Thread th;

        public SplashScreen()
        {
            
            InitializeComponent();
            backgroundWorker.DoWork += backgroundWorker_DoWork;
            backgroundWorker.RunWorkerCompleted += backgroundWorker_Done;
            backgroundWorker.RunWorkerAsync();
            /*for(int i = 0; i <=100; i=+10)
            {
                progressBar1.Value = i;
                Task.Delay(500);
            }*/
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            uploader.setup(false);
        }

        private void backgroundWorker_Done(object sender, RunWorkerCompletedEventArgs e)
        {                      
            th = new Thread(openMainWindow);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
            this.Close();
        }

        private void openMainWindow(object obj)
        {
            Application.Run(new MainWindow(uploader));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            panel2.Width += 3;
            if (panel2.Width >= (panel1.Width*0.7))
            {
                timer1.Enabled = false;
            }
        }
    }
}
